misc = {}

local S

if minetest.get_translator ~= nil then
    S = minetest.get_translator(minetest.get_current_modname())
else
    S = function(str)
        return(str)
    end
end

local modpath = minetest.get_modpath("misc")
dofile(modpath.."/mapfix.lua")

minetest.register_craft({
	output = "wool:white",
	type = "shapeless",
	recipe = {"rhotator:cube"},
})

technic.register_threshing_recipe({input = {"nettle:nettle 99"}, output = { "cottages:hay_mat 20", "nettle:cleavers 5", "nettle:impatiens 5", "nettle:carduus 5"}, time = 16})
technic.register_threshing_recipe({input = {"nettle:cleavers 99"}, output = {"cottages:hay_mat 20", "nettle:giant_hogweed 5", "nettle:carduus 5", "nettle:scotch_broom"}, time = 16})
technic.register_threshing_recipe({input = {"nettle:impatiens 99"}, output = {"underch:moss 15"}, time = 16})

technic.register_separating_recipe({ input = {"underch:mossy_gravel"}, output = {"default:gravel", "underch:moss"}, time = 8 })
technic.register_separating_recipe({ input = {"underch:mossy_dirt"}, output = {"default:clay"}, time = 8})
technic.register_separating_recipe({ input = {"underch:obscurite 99"}, output = {"default:lava_source 2", "underch:afualite 49", "underch:hektorite 49"}, time = 30})

technic.register_threshing_recipe({input = {"farming:cotton"}, output = {"farming:string 2"}, time = 8})
technic.register_threshing_recipe({input = {"farming:rice"}, output = {"farming:seed_rice"}, time = 8})
technic.register_threshing_recipe({input = {"nettle:impatiens 99"}, output = {"underch:moss 25"}, time = 16})
technic.register_threshing_recipe({input = {"nettle:carduus 50"}, output = {"underch:dead_bush 25"}, time = 16})
technic.register_threshing_recipe({input = {"nettle:giant_hogweed 99"}, output = {"underch:mould 20"}, time = 16})
technic.register_threshing_recipe({input = {"nettle:scotch_broom 50"}, output = {"underch:underground_bush 30"}, time = 16})

technic.register_extractor_recipe({input = {"underch:black_mushroom 10"}, output = {"underch:black_bucket"}, time = 40})

minetest.register_craft({
	output = "streets:mark_solid_yellow_center_line 50",
	recipe = {
	{"dye:yellow", "default:paper", ""},
	{"dye:yellow", "default:paper", ""},
	{"dye:yellow", "default:paper", ""},
	}
})

minetest.register_craft({
	output = "streets:mark_dashed_yellow_center_line 50",
	recipe = {
	    {"default:paper", "dye:yellow", ""},
	    {"", "", ""},
	    {"default:paper", "dye:yellow", ""},
	    }
})

minetest.register_craft({
	output = "technic:plastic_tiles 9",
	recipe = {
	    {"technic:plastic_block", "technic:plastic_block", "technic:plastic_block"},
	    {"technic:plastic_block", "technic:plastic_block", "technic:plastic_block"},
	    {"technic:plastic_block", "technic:plastic_block", "technic:plastic_block"},
	    }
})

minetest.register_craft({
	output = "technic:plastic_cross 9",
	recipe = {
	    {"technic:plastic_tiles", "technic:plastic_tiles", "technic:plastic_tiles"},
	    {"technic:plastic_tiles", "technic:plastic_tiles", "technic:plastic_tiles"},
	    {"technic:plastic_tiles", "technic:plastic_tiles", "technic:plastic_tiles"},
	    }
})

minetest.register_craft({
	output = "technic:plastic_waves 9",
	recipe = {
	    {"technic:plastic_cross", "technic:plastic_cross", "technic:plastic_cross"},
	    {"technic:plastic_cross", "technic:plastic_cross", "technic:plastic_cross"},
	    {"technic:plastic_cross", "technic:plastic_cross", "technic:plastic_cross"},
	    }
})

minetest.register_craft({
	output = "technic:plastic_clean 9",
	recipe = {
	    {"technic:plastic_waves", "technic:plastic_waves", "technic:plastic_waves"},
	    {"technic:plastic_waves", "technic:plastic_waves", "technic:plastic_waves"},
	    {"technic:plastic_waves", "technic:plastic_waves", "technic:plastic_waves"},
	    }
})

minetest.register_craft({
	output = "advtrains:engine_japan",
	recipe = {
	    {"xtraores:titanium_block", "xtraores:titanium_block", "xtraores:titanium_block"},
	    {"xtraores:titanium_block", "dye:white", "xtraores:titanium_block"},
	    {"xtraores:titanium_block", "xtraores:titanium_block", "xtraores:titanium_block"},
	    }
})

minetest.register_craft({
	output = "advtrains:wagon_japan",
	recipe = {
	    {"default:steelblock", "xtraores:titanium_block", "default:steelblock"},
	    {"xtraores:titanium_block", "dye:white", "xtraores:titanium_block"},
	    {"xtraores:titanium_block", "xtraores:titanium_block", "xtraores:titanium_block"},
	    }
})

minetest.register_craft({
	output = "streets:mark_dashed_yellow_center_line 50",
	recipe = {
	    {"default:paper", "dye:yellow", ""},
	    {"", "", ""},
	    {"default:paper", "dye:yellow", ""},
	    }
})

minetest.register_craft({
	output = "xpanes:bar_flat 18",
	recipe = {
	    {"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
	    {"default:steel_ingot", "", "default:steel_ingot"},
	    {"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
	    }
})

minetest.register_craft({
	output = "streets:mark_solid_yellow_side_line 20",
	recipe = {
	    {"", "dye:yellow", ""},
	    {"", "default:paper", ""},
	    {"", "dye:yellow", ""},
	    }
})

minetest.register_craft({
	output = "streets:mark_solid_white_side_line 20",
	recipe = {
	    {"", "dye:white", ""},
	    {"", "default:paper", ""},
	    {"", "dye:white", ""},
	    }
})

minetest.register_craft({
	output = "streets:mark_dashed_white_center_line 50",
	recipe = {
	    {"default:paper", "dye:white", ""},
	    {"", "", ""},
	    {"default:paper", "dye:white", ""},
	    }
})

minetest.register_craft({
	output = "streets:mark_solid_white_center_line 50",
	recipe = {
	{"dye:white", "default:paper", ""},
	{"dye:white", "default:paper", ""},
	{"dye:white", "default:paper", ""},
	}
})

minetest.register_craft({
	output = "3dforniture:bars 10",
	recipe = {
	{"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
	{"default:steel_ingot", "", "default:steel_ingot"},
	{"default:steel_ingot", "", "default:steel_ingot"},
	}
})

minetest.register_node("misc:0", {
	description = "0",
	paramtype = "light",
	pointable = false,
	walkable = true,
	diggable = false,
	on_blast = function(pos, intensity)
	end,
	groups = {not_in_creative_inventory = 1},
	sunlight_propagates = true,
	drawtype = "airlike",
})

minetest.register_node("misc:obscurite", {
	description = "Unpointable Obscurite",
	tiles = {"underch_obscurite.png"},
	groups = {},
	pointable = false,
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_craftitem("misc:PC_share", {
	description = "PC Share",
	inventory_image = "default_paper.png",
	groups = {flammable = 3},
	stack_max = 10000,
})

minetest.register_craftitem("misc:TP_share", {
	description = "Tunnelers' Paradise Share",
	inventory_image = "default_paper.png",
	groups = {flammable = 3},
	stack_max = 10000,
})

minetest.register_craftitem("misc:TS_share", {
	description = "Tunnelers' Safari Share",
	inventory_image = "default_paper.png",
	groups = {flammable = 3},
	stack_max = 10000,
})

minetest.register_craftitem("misc:TO_share", {
	description = "Tunnelers' Outback Share",
	inventory_image = "default_paper.png",
	groups = {flammable = 3},
	stack_max = 10000,
})

minetest.register_craftitem("misc:CIIRC_share", {
	description = "CIIRC Share",
	inventory_image = "default_paper.png",
	groups = {flammable = 3},
	stack_max = 10000,
})

local function mylength(table)
	local foo = 0
	for _ in pairs(table) do foo = foo + 1 end
	return foo
end

minetest.register_chatcommand("features", {
   description = "Display counts of registered features",
   privs = {},
   params = "",
   func = function(name, param)
      minetest.chat_send_player(name, string.format("Registered nodes: %d", mylength(minetest.registered_nodes)))
      minetest.chat_send_player(name, string.format("Registered craftitems: %d", mylength(minetest.registered_craftitems)))
      minetest.chat_send_player(name, string.format("Registered tools: %d", mylength(minetest.registered_tools)))
      minetest.chat_send_player(name, string.format("Registered entities: %d", mylength(minetest.registered_entities)))
      minetest.chat_send_player(name, string.format("Registered ABMs: %d", #(minetest.registered_abms)))
      minetest.chat_send_player(name, string.format("Registered LBMs: %d", #(minetest.registered_lbms)))
   end,
})

minetest.register_node("misc:gsml1",{
    description = "Map of Grapeyard Metro",
    drawtype = "signlike",
    visual_scale=3.0,
    wield_image = "gsml1.png",
    inventory_image= "gsml1.png",
    tiles = { "gsml1.png" },
    sunlight_propagates = true,
    walkable = false,
    paramtype = "light",
    paramtype2 = "wallmounted",
    light_source=10,
    selection_box = {
        type = "wallmounted"
    },
    groups = {choppy=2,dig_immediate=3,attached_node=1, picture=1},
    legacy_wallmounted = true,
})

minetest.register_node("misc:ta_train_map",{
    description = "Tunnelers' Abyss Train Map",
    drawtype = "signlike",
    visual_scale=3.0,
    wield_image = "tamp.png",
    inventory_image= "tamp.png",
    tiles = { "tamp.png" },
    sunlight_propagates = true,
    walkable = false,
    paramtype = "light",
    paramtype2 = "wallmounted",
    light_source=10,
    selection_box = {
        type = "wallmounted"
    },
    groups = {choppy=2,dig_immediate=3,attached_node=1, picture=1},
    legacy_wallmounted = true,
})

minetest.register_chatcommand("abm_list", {
   description = "Display names of registered ABMs",
   privs = {},
   params = "",
   func = function(name, param)
	for _, abm in ipairs(minetest.registered_abms) do
	    nodenames = ""
	    if type(abm.nodenames) == "table" then
	        for _, node in ipairs(abm.nodenames) do
                    nodenames = nodenames .. " " .. node
	        end
	    elseif type(abm.nodenames) == "string" then
	        nodenames = abm.nodenames
	    else
	        nodenames = "WRONG TYPE: "..type(abm.nodenames)
	    end
	    minetest.chat_send_player(name, string.format("interval:%d chance:%d nodes:{%s}", abm.interval, abm.chance, nodenames))
	end
   end,
})

minetest.register_craftitem("misc:fries", {
	description = ("French Fries"),
	inventory_image = "fries.png",
	on_use = minetest.item_eat(6),
})

minetest.register_node("misc:flint_and_mortar", {
	description = S("Flint and Mortar"),
	tiles = {"flint_knapping.png"},
	paramtype2 = "facedir",
	place_param2 = 0,
	is_ground_content = false,
	groups = {oddly_breakable_by_hand = 1, flammable = 3},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("misc:snowbell", {
	description = S("Snowbell"),
	tiles = {"snowbell.png"},
	inventory_image = "snowbell.png",
	wield_image = "snowbell.png",
	drawtype = "plantlike",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	stack_max = 99,
	groups = {snappy = 3, attached_node = 1, flammable = 1},
	sounds = default.node_sound_leaves_defaults(),
	on_use = minetest.item_eat(1),
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, -1 / 16, 4 / 16},
	}
})

minetest.register_craft({
	output = "misc:flint_and_mortar 4",
	recipe = {
	{"default:flint", "default:flint", "default:flint"},
	{"default:flint", "default:clay", "default:flint"},
	{"default:flint", "default:flint", "default:flint"},
	}
})

minetest.register_craft({
	output = "misc:snowbell",
	recipe = {
	{"", "ethereal:fire_flower", ""},
	{"", "default:snow", ""},
	{"", "ethereal:snowygrass", ""},
	}
})

technic.register_alloy_recipe({input = {"farming:potato", "basic_materials:oil_extract"}, output = "misc:fries", time = 3})
technic.register_alloy_recipe({input = {"farming:potato", "technic:cottonseed_oil"}, output = "misc:fries", time = 3})
technic.register_alloy_recipe({input = {"ethereal:fire_flower", "default:snow"}, output = "misc:snowbell", time = 8})

